package com.nashtech.repository;

import com.nashtech.domain.Expertise;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Expertise entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExpertiseRepository extends JpaRepository<Expertise, Long> {

}
