package com.nashtech.repository;

import com.nashtech.domain.SuggestSkill;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SuggestSkill entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SuggestSkillRepository extends JpaRepository<SuggestSkill, Long> {

}
