package com.nashtech.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Expertise.
 */
@Entity
@Table(name = "expertise")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Expertise implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "skill")
    private String skill;

    @Column(name = "expertise")
    private String expertise;

    @Column(name = "additional")
    private String additional;

    @ManyToOne
    @JsonIgnoreProperties("expertise")
    private Job job;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSkill() {
        return skill;
    }

    public Expertise skill(String skill) {
        this.skill = skill;
        return this;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getExpertise() {
        return expertise;
    }

    public Expertise expertise(String expertise) {
        this.expertise = expertise;
        return this;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public String getAdditional() {
        return additional;
    }

    public Expertise additional(String additional) {
        this.additional = additional;
        return this;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    public Job getJob() {
        return job;
    }

    public Expertise job(Job job) {
        this.job = job;
        return this;
    }

    public void setJob(Job job) {
        this.job = job;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Expertise)) {
            return false;
        }
        return id != null && id.equals(((Expertise) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Expertise{" +
            "id=" + getId() +
            ", skill='" + getSkill() + "'" +
            ", expertise='" + getExpertise() + "'" +
            ", additional='" + getAdditional() + "'" +
            "}";
    }
}
