package com.nashtech.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import com.nashtech.domain.enumeration.TermType;

import com.nashtech.domain.enumeration.TypeProject;

import com.nashtech.domain.enumeration.Viewer;

import com.nashtech.domain.enumeration.JobStatus;

import com.nashtech.domain.enumeration.Payment;

import com.nashtech.domain.enumeration.Level;

import com.nashtech.domain.enumeration.TimeProject;

import com.nashtech.domain.enumeration.TimeRequirement;

/**
 * A Job.
 */
@Entity
@Table(name = "job")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Job implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "create_at", nullable = false)
    private Instant createAt;

    @NotNull
    @Column(name = "update_at", nullable = false)
    private Instant updateAt;

    @NotNull
    @Column(name = "user_create_id", nullable = false)
    private Long userCreateID;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "term_type", nullable = false)
    private TermType termType;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @NotNull
    @Column(name = "category_item_id", nullable = false)
    private Long categoryItemID;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @Column(name = "image_content_type")
    private String imageContentType;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type_project", nullable = false)
    private TypeProject typeProject;

    @NotNull
    @Column(name = "num_of_member", nullable = false)
    private Integer numOfMember;

    @Column(name = "cover_letter")
    private Boolean coverLetter;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "viewer", nullable = false)
    private Viewer viewer;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private JobStatus status;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "payment", nullable = false)
    private Payment payment;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_level", nullable = false)
    private Level level;

    @Enumerated(EnumType.STRING)
    @Column(name = "time_project")
    private TimeProject timeProject;

    @Enumerated(EnumType.STRING)
    @Column(name = "time_requirement")
    private TimeRequirement timeRequirement;

    @Column(name = "specific_budget")
    private Integer specificBudget;

    @OneToOne
    @JoinColumn(unique = true)
    private Qualification qualification;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getCreateAt() {
        return createAt;
    }

    public Job createAt(Instant createAt) {
        this.createAt = createAt;
        return this;
    }

    public void setCreateAt(Instant createAt) {
        this.createAt = createAt;
    }

    public Instant getUpdateAt() {
        return updateAt;
    }

    public Job updateAt(Instant updateAt) {
        this.updateAt = updateAt;
        return this;
    }

    public void setUpdateAt(Instant updateAt) {
        this.updateAt = updateAt;
    }

    public Long getUserCreateID() {
        return userCreateID;
    }

    public Job userCreateID(Long userCreateID) {
        this.userCreateID = userCreateID;
        return this;
    }

    public void setUserCreateID(Long userCreateID) {
        this.userCreateID = userCreateID;
    }

    public TermType getTermType() {
        return termType;
    }

    public Job termType(TermType termType) {
        this.termType = termType;
        return this;
    }

    public void setTermType(TermType termType) {
        this.termType = termType;
    }

    public String getTitle() {
        return title;
    }

    public Job title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getCategoryItemID() {
        return categoryItemID;
    }

    public Job categoryItemID(Long categoryItemID) {
        this.categoryItemID = categoryItemID;
        return this;
    }

    public void setCategoryItemID(Long categoryItemID) {
        this.categoryItemID = categoryItemID;
    }

    public String getDescription() {
        return description;
    }

    public Job description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getImage() {
        return image;
    }

    public Job image(byte[] image) {
        this.image = image;
        return this;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public Job imageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
        return this;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public TypeProject getTypeProject() {
        return typeProject;
    }

    public Job typeProject(TypeProject typeProject) {
        this.typeProject = typeProject;
        return this;
    }

    public void setTypeProject(TypeProject typeProject) {
        this.typeProject = typeProject;
    }

    public Integer getNumOfMember() {
        return numOfMember;
    }

    public Job numOfMember(Integer numOfMember) {
        this.numOfMember = numOfMember;
        return this;
    }

    public void setNumOfMember(Integer numOfMember) {
        this.numOfMember = numOfMember;
    }

    public Boolean isCoverLetter() {
        return coverLetter;
    }

    public Job coverLetter(Boolean coverLetter) {
        this.coverLetter = coverLetter;
        return this;
    }

    public void setCoverLetter(Boolean coverLetter) {
        this.coverLetter = coverLetter;
    }

    public Viewer getViewer() {
        return viewer;
    }

    public Job viewer(Viewer viewer) {
        this.viewer = viewer;
        return this;
    }

    public void setViewer(Viewer viewer) {
        this.viewer = viewer;
    }

    public JobStatus getStatus() {
        return status;
    }

    public Job status(JobStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public Payment getPayment() {
        return payment;
    }

    public Job payment(Payment payment) {
        this.payment = payment;
        return this;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Level getLevel() {
        return level;
    }

    public Job level(Level level) {
        this.level = level;
        return this;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public TimeProject getTimeProject() {
        return timeProject;
    }

    public Job timeProject(TimeProject timeProject) {
        this.timeProject = timeProject;
        return this;
    }

    public void setTimeProject(TimeProject timeProject) {
        this.timeProject = timeProject;
    }

    public TimeRequirement getTimeRequirement() {
        return timeRequirement;
    }

    public Job timeRequirement(TimeRequirement timeRequirement) {
        this.timeRequirement = timeRequirement;
        return this;
    }

    public void setTimeRequirement(TimeRequirement timeRequirement) {
        this.timeRequirement = timeRequirement;
    }

    public Integer getSpecificBudget() {
        return specificBudget;
    }

    public Job specificBudget(Integer specificBudget) {
        this.specificBudget = specificBudget;
        return this;
    }

    public void setSpecificBudget(Integer specificBudget) {
        this.specificBudget = specificBudget;
    }

    public Qualification getQualification() {
        return qualification;
    }

    public Job qualification(Qualification qualification) {
        this.qualification = qualification;
        return this;
    }

    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Job)) {
            return false;
        }
        return id != null && id.equals(((Job) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Job{" +
            "id=" + getId() +
            ", createAt='" + getCreateAt() + "'" +
            ", updateAt='" + getUpdateAt() + "'" +
            ", userCreateID=" + getUserCreateID() +
            ", termType='" + getTermType() + "'" +
            ", title='" + getTitle() + "'" +
            ", categoryItemID=" + getCategoryItemID() +
            ", description='" + getDescription() + "'" +
            ", image='" + getImage() + "'" +
            ", imageContentType='" + getImageContentType() + "'" +
            ", typeProject='" + getTypeProject() + "'" +
            ", numOfMember=" + getNumOfMember() +
            ", coverLetter='" + isCoverLetter() + "'" +
            ", viewer='" + getViewer() + "'" +
            ", status='" + getStatus() + "'" +
            ", payment='" + getPayment() + "'" +
            ", level='" + getLevel() + "'" +
            ", timeProject='" + getTimeProject() + "'" +
            ", timeRequirement='" + getTimeRequirement() + "'" +
            ", specificBudget=" + getSpecificBudget() +
            "}";
    }
}
