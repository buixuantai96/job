package com.nashtech.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import com.nashtech.domain.enumeration.FreelancerType;

import com.nashtech.domain.enumeration.HoursBilledonUpwork;

import com.nashtech.domain.enumeration.JobSuccessScore;

import com.nashtech.domain.enumeration.RisingTalent;

import com.nashtech.domain.enumeration.Location;

import com.nashtech.domain.enumeration.EnglishLevel;

import com.nashtech.domain.enumeration.Group;

/**
 * A Qualification.
 */
@Entity
@Table(name = "qualification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Qualification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "freelancertype", nullable = false)
    private FreelancerType freelancertype;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "hoursbilledonupwork", nullable = false)
    private HoursBilledonUpwork hoursbilledonupwork;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "jobsuccessscore", nullable = false)
    private JobSuccessScore jobsuccessscore;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "risingtalent", nullable = false)
    private RisingTalent risingtalent;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "location", nullable = false)
    private Location location;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "englishlevel", nullable = false)
    private EnglishLevel englishlevel;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_group", nullable = false)
    private Group group;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FreelancerType getFreelancertype() {
        return freelancertype;
    }

    public Qualification freelancertype(FreelancerType freelancertype) {
        this.freelancertype = freelancertype;
        return this;
    }

    public void setFreelancertype(FreelancerType freelancertype) {
        this.freelancertype = freelancertype;
    }

    public HoursBilledonUpwork getHoursbilledonupwork() {
        return hoursbilledonupwork;
    }

    public Qualification hoursbilledonupwork(HoursBilledonUpwork hoursbilledonupwork) {
        this.hoursbilledonupwork = hoursbilledonupwork;
        return this;
    }

    public void setHoursbilledonupwork(HoursBilledonUpwork hoursbilledonupwork) {
        this.hoursbilledonupwork = hoursbilledonupwork;
    }

    public JobSuccessScore getJobsuccessscore() {
        return jobsuccessscore;
    }

    public Qualification jobsuccessscore(JobSuccessScore jobsuccessscore) {
        this.jobsuccessscore = jobsuccessscore;
        return this;
    }

    public void setJobsuccessscore(JobSuccessScore jobsuccessscore) {
        this.jobsuccessscore = jobsuccessscore;
    }

    public RisingTalent getRisingtalent() {
        return risingtalent;
    }

    public Qualification risingtalent(RisingTalent risingtalent) {
        this.risingtalent = risingtalent;
        return this;
    }

    public void setRisingtalent(RisingTalent risingtalent) {
        this.risingtalent = risingtalent;
    }

    public Location getLocation() {
        return location;
    }

    public Qualification location(Location location) {
        this.location = location;
        return this;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public EnglishLevel getEnglishlevel() {
        return englishlevel;
    }

    public Qualification englishlevel(EnglishLevel englishlevel) {
        this.englishlevel = englishlevel;
        return this;
    }

    public void setEnglishlevel(EnglishLevel englishlevel) {
        this.englishlevel = englishlevel;
    }

    public Group getGroup() {
        return group;
    }

    public Qualification group(Group group) {
        this.group = group;
        return this;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Qualification)) {
            return false;
        }
        return id != null && id.equals(((Qualification) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Qualification{" +
            "id=" + getId() +
            ", freelancertype='" + getFreelancertype() + "'" +
            ", hoursbilledonupwork='" + getHoursbilledonupwork() + "'" +
            ", jobsuccessscore='" + getJobsuccessscore() + "'" +
            ", risingtalent='" + getRisingtalent() + "'" +
            ", location='" + getLocation() + "'" +
            ", englishlevel='" + getEnglishlevel() + "'" +
            ", group='" + getGroup() + "'" +
            "}";
    }
}
