package com.nashtech.domain.enumeration;

/**
 * The TimeRequirement enumeration.
 */
public enum TimeRequirement {
    MORE_THAN_30HOURS_PERWEEK, LESS_THAN_30HOURS_PERWEEK, DONT_KNOW_YET
}
