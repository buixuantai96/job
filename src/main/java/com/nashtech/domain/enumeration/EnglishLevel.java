package com.nashtech.domain.enumeration;

/**
 * The EnglishLevel enumeration.
 */
public enum EnglishLevel {
    ANY, BASIC, CONVERSATIONAL, FLUENT, NATIVE_OR_BILINGUAL
}
