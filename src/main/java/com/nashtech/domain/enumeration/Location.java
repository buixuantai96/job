package com.nashtech.domain.enumeration;

/**
 * The Location enumeration.
 */
public enum Location {
    ANY, AFRICA, AMERICA, ANTARCTICA, ASIA, EUROPE, OCIANIA
}
