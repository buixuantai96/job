package com.nashtech.domain.enumeration;

/**
 * The RisingTalent enumeration.
 */
public enum RisingTalent {
    INCLUDE, DONT
}
