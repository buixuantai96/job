package com.nashtech.domain.enumeration;

/**
 * The Viewer enumeration.
 */
public enum Viewer {
    ANYONE, ONLY_UPWORK_FREELANCER, INVITE_ONLY
}
