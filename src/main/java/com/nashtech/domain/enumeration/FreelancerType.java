package com.nashtech.domain.enumeration;

/**
 * The FreelancerType enumeration.
 */
public enum FreelancerType {
    NO_PREFER, INDEPENDENT, AGENCY
}
