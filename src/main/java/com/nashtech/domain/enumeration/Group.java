package com.nashtech.domain.enumeration;

/**
 * The Group enumeration.
 */
public enum Group {
    NO_PREFERENT, AWEBER_EMAIL_MARTKETING, PLIGG, SOCIAL_IMPACT_AGENCY, US_MILITARY_VETERANTS
}
