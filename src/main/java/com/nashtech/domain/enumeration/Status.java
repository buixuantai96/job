package com.nashtech.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    PENDING, ACCEPT, DECLINE
}
