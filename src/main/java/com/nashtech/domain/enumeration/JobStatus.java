package com.nashtech.domain.enumeration;

/**
 * The JobStatus enumeration.
 */
public enum JobStatus {
    DRAFT, PENDING, ACCEPTED, DENIED
}
