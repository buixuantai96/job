package com.nashtech.domain.enumeration;

/**
 * The HoursBilledonUpwork enumeration.
 */
public enum HoursBilledonUpwork {
    ANY, ONE, ONEHUNDRED, FIVEHUNDRED, ONETHOUSAND
}
