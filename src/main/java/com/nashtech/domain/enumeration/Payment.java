package com.nashtech.domain.enumeration;

/**
 * The Payment enumeration.
 */
public enum Payment {
    PAY_BY_THE_HOUR, PAY_A_FIXED_PRICE
}
