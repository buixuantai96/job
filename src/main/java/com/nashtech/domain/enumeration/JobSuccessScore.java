package com.nashtech.domain.enumeration;

/**
 * The JobSuccessScore enumeration.
 */
public enum JobSuccessScore {
    ANY, NINETY_PERCENT, EIGHTY_PERCENT
}
