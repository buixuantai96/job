package com.nashtech.domain.enumeration;

/**
 * The TimeProject enumeration.
 */
public enum TimeProject {
    MOTE_THAN_SIX_MONTHS, THREE_TO_SIX_MONTHS, ONE_TO_THREE_MONTHS, LESS_THAN_ONE_MONTH
}
