package com.nashtech.domain.enumeration;

/**
 * The Level enumeration.
 */
public enum Level {
    ENTRY, INTERMEDIATE, EXPERT
}
