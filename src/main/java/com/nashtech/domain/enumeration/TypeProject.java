package com.nashtech.domain.enumeration;

/**
 * The TypeProject enumeration.
 */
public enum TypeProject {
    ONE_TIME, ON_GOING, NOT_SURE
}
