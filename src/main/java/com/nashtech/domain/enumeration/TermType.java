package com.nashtech.domain.enumeration;

/**
 * The TermType enumeration.
 */
public enum TermType {
    LONG_TERM, SHORT_TERM
}
