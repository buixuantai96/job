package com.nashtech.service.mapper;

import com.nashtech.domain.*;
import com.nashtech.service.dto.QualificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Qualification} and its DTO {@link QualificationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface QualificationMapper extends EntityMapper<QualificationDTO, Qualification> {



    default Qualification fromId(Long id) {
        if (id == null) {
            return null;
        }
        Qualification qualification = new Qualification();
        qualification.setId(id);
        return qualification;
    }
}
