package com.nashtech.service.mapper;

import com.nashtech.domain.*;
import com.nashtech.service.dto.JobDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Job} and its DTO {@link JobDTO}.
 */
@Mapper(componentModel = "spring", uses = {QualificationMapper.class})
public interface JobMapper extends EntityMapper<JobDTO, Job> {

    @Mapping(source = "qualification.id", target = "qualificationId")
    JobDTO toDto(Job job);

    @Mapping(source = "qualificationId", target = "qualification")
    Job toEntity(JobDTO jobDTO);

    default Job fromId(Long id) {
        if (id == null) {
            return null;
        }
        Job job = new Job();
        job.setId(id);
        return job;
    }
}
