package com.nashtech.service.mapper;

import com.nashtech.domain.*;
import com.nashtech.service.dto.SuggestSkillDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SuggestSkill} and its DTO {@link SuggestSkillDTO}.
 */
@Mapper(componentModel = "spring", uses = {ExpertiseMapper.class})
public interface SuggestSkillMapper extends EntityMapper<SuggestSkillDTO, SuggestSkill> {

    @Mapping(source = "expertise.id", target = "expertiseId")
    SuggestSkillDTO toDto(SuggestSkill suggestSkill);

    @Mapping(source = "expertiseId", target = "expertise")
    SuggestSkill toEntity(SuggestSkillDTO suggestSkillDTO);

    default SuggestSkill fromId(Long id) {
        if (id == null) {
            return null;
        }
        SuggestSkill suggestSkill = new SuggestSkill();
        suggestSkill.setId(id);
        return suggestSkill;
    }
}
