package com.nashtech.service.mapper;

import com.nashtech.domain.*;
import com.nashtech.service.dto.ExpertiseDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Expertise} and its DTO {@link ExpertiseDTO}.
 */
@Mapper(componentModel = "spring", uses = {JobMapper.class})
public interface ExpertiseMapper extends EntityMapper<ExpertiseDTO, Expertise> {

    @Mapping(source = "job.id", target = "jobId")
    ExpertiseDTO toDto(Expertise expertise);

    @Mapping(source = "jobId", target = "job")
    Expertise toEntity(ExpertiseDTO expertiseDTO);

    default Expertise fromId(Long id) {
        if (id == null) {
            return null;
        }
        Expertise expertise = new Expertise();
        expertise.setId(id);
        return expertise;
    }
}
