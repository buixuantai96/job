package com.nashtech.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.nashtech.domain.enumeration.FreelancerType;
import com.nashtech.domain.enumeration.HoursBilledonUpwork;
import com.nashtech.domain.enumeration.JobSuccessScore;
import com.nashtech.domain.enumeration.RisingTalent;
import com.nashtech.domain.enumeration.Location;
import com.nashtech.domain.enumeration.EnglishLevel;
import com.nashtech.domain.enumeration.Group;

/**
 * A DTO for the {@link com.nashtech.domain.Qualification} entity.
 */
public class QualificationDTO implements Serializable {

    private Long id;

    @NotNull
    private FreelancerType freelancertype;

    @NotNull
    private HoursBilledonUpwork hoursbilledonupwork;

    @NotNull
    private JobSuccessScore jobsuccessscore;

    @NotNull
    private RisingTalent risingtalent;

    @NotNull
    private Location location;

    @NotNull
    private EnglishLevel englishlevel;

    @NotNull
    private Group group;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FreelancerType getFreelancertype() {
        return freelancertype;
    }

    public void setFreelancertype(FreelancerType freelancertype) {
        this.freelancertype = freelancertype;
    }

    public HoursBilledonUpwork getHoursbilledonupwork() {
        return hoursbilledonupwork;
    }

    public void setHoursbilledonupwork(HoursBilledonUpwork hoursbilledonupwork) {
        this.hoursbilledonupwork = hoursbilledonupwork;
    }

    public JobSuccessScore getJobsuccessscore() {
        return jobsuccessscore;
    }

    public void setJobsuccessscore(JobSuccessScore jobsuccessscore) {
        this.jobsuccessscore = jobsuccessscore;
    }

    public RisingTalent getRisingtalent() {
        return risingtalent;
    }

    public void setRisingtalent(RisingTalent risingtalent) {
        this.risingtalent = risingtalent;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public EnglishLevel getEnglishlevel() {
        return englishlevel;
    }

    public void setEnglishlevel(EnglishLevel englishlevel) {
        this.englishlevel = englishlevel;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        QualificationDTO qualificationDTO = (QualificationDTO) o;
        if (qualificationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), qualificationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "QualificationDTO{" +
            "id=" + getId() +
            ", freelancertype='" + getFreelancertype() + "'" +
            ", hoursbilledonupwork='" + getHoursbilledonupwork() + "'" +
            ", jobsuccessscore='" + getJobsuccessscore() + "'" +
            ", risingtalent='" + getRisingtalent() + "'" +
            ", location='" + getLocation() + "'" +
            ", englishlevel='" + getEnglishlevel() + "'" +
            ", group='" + getGroup() + "'" +
            "}";
    }
}
