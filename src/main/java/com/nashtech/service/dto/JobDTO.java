package com.nashtech.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import com.nashtech.domain.enumeration.TermType;
import com.nashtech.domain.enumeration.TypeProject;
import com.nashtech.domain.enumeration.Viewer;
import com.nashtech.domain.enumeration.JobStatus;
import com.nashtech.domain.enumeration.Payment;
import com.nashtech.domain.enumeration.Level;
import com.nashtech.domain.enumeration.TimeProject;
import com.nashtech.domain.enumeration.TimeRequirement;

/**
 * A DTO for the {@link com.nashtech.domain.Job} entity.
 */
public class JobDTO implements Serializable {

    private Long id;

    @NotNull
    private Instant createAt;

    @NotNull
    private Instant updateAt;

    @NotNull
    private Long userCreateID;

    @NotNull
    private TermType termType;

    @NotNull
    private String title;

    @NotNull
    private Long categoryItemID;

    @NotNull
    private String description;

    @Lob
    private byte[] image;

    private String imageContentType;
    @NotNull
    private TypeProject typeProject;

    @NotNull
    private Integer numOfMember;

    private Boolean coverLetter;

    @NotNull
    private Viewer viewer;

    private JobStatus status;

    @NotNull
    private Payment payment;

    @NotNull
    private Level level;

    private TimeProject timeProject;

    private TimeRequirement timeRequirement;

    private Integer specificBudget;


    private Long qualificationId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Instant createAt) {
        this.createAt = createAt;
    }

    public Instant getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Instant updateAt) {
        this.updateAt = updateAt;
    }

    public Long getUserCreateID() {
        return userCreateID;
    }

    public void setUserCreateID(Long userCreateID) {
        this.userCreateID = userCreateID;
    }

    public TermType getTermType() {
        return termType;
    }

    public void setTermType(TermType termType) {
        this.termType = termType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getCategoryItemID() {
        return categoryItemID;
    }

    public void setCategoryItemID(Long categoryItemID) {
        this.categoryItemID = categoryItemID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public TypeProject getTypeProject() {
        return typeProject;
    }

    public void setTypeProject(TypeProject typeProject) {
        this.typeProject = typeProject;
    }

    public Integer getNumOfMember() {
        return numOfMember;
    }

    public void setNumOfMember(Integer numOfMember) {
        this.numOfMember = numOfMember;
    }

    public Boolean isCoverLetter() {
        return coverLetter;
    }

    public void setCoverLetter(Boolean coverLetter) {
        this.coverLetter = coverLetter;
    }

    public Viewer getViewer() {
        return viewer;
    }

    public void setViewer(Viewer viewer) {
        this.viewer = viewer;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public TimeProject getTimeProject() {
        return timeProject;
    }

    public void setTimeProject(TimeProject timeProject) {
        this.timeProject = timeProject;
    }

    public TimeRequirement getTimeRequirement() {
        return timeRequirement;
    }

    public void setTimeRequirement(TimeRequirement timeRequirement) {
        this.timeRequirement = timeRequirement;
    }

    public Integer getSpecificBudget() {
        return specificBudget;
    }

    public void setSpecificBudget(Integer specificBudget) {
        this.specificBudget = specificBudget;
    }

    public Long getQualificationId() {
        return qualificationId;
    }

    public void setQualificationId(Long qualificationId) {
        this.qualificationId = qualificationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JobDTO jobDTO = (JobDTO) o;
        if (jobDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jobDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JobDTO{" +
            "id=" + getId() +
            ", createAt='" + getCreateAt() + "'" +
            ", updateAt='" + getUpdateAt() + "'" +
            ", userCreateID=" + getUserCreateID() +
            ", termType='" + getTermType() + "'" +
            ", title='" + getTitle() + "'" +
            ", categoryItemID=" + getCategoryItemID() +
            ", description='" + getDescription() + "'" +
            ", image='" + getImage() + "'" +
            ", typeProject='" + getTypeProject() + "'" +
            ", numOfMember=" + getNumOfMember() +
            ", coverLetter='" + isCoverLetter() + "'" +
            ", viewer='" + getViewer() + "'" +
            ", status='" + getStatus() + "'" +
            ", payment='" + getPayment() + "'" +
            ", level='" + getLevel() + "'" +
            ", timeProject='" + getTimeProject() + "'" +
            ", timeRequirement='" + getTimeRequirement() + "'" +
            ", specificBudget=" + getSpecificBudget() +
            ", qualification=" + getQualificationId() +
            "}";
    }
}
