package com.nashtech.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.nashtech.domain.enumeration.Status;

/**
 * A DTO for the {@link com.nashtech.domain.SuggestSkill} entity.
 */
public class SuggestSkillDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String description;

    private String link;

    private Status status;


    private Long expertiseId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getExpertiseId() {
        return expertiseId;
    }

    public void setExpertiseId(Long expertiseId) {
        this.expertiseId = expertiseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SuggestSkillDTO suggestSkillDTO = (SuggestSkillDTO) o;
        if (suggestSkillDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), suggestSkillDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SuggestSkillDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", link='" + getLink() + "'" +
            ", status='" + getStatus() + "'" +
            ", expertise=" + getExpertiseId() +
            "}";
    }
}
