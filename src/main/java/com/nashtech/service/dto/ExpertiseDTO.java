package com.nashtech.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.nashtech.domain.Expertise} entity.
 */
public class ExpertiseDTO implements Serializable {

    private Long id;

    private String skill;

    private String expertise;

    private String additional;


    private Long jobId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getExpertise() {
        return expertise;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ExpertiseDTO expertiseDTO = (ExpertiseDTO) o;
        if (expertiseDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), expertiseDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ExpertiseDTO{" +
            "id=" + getId() +
            ", skill='" + getSkill() + "'" +
            ", expertise='" + getExpertise() + "'" +
            ", additional='" + getAdditional() + "'" +
            ", job=" + getJobId() +
            "}";
    }
}
