package com.nashtech.service;

import com.nashtech.domain.SuggestSkill;
import com.nashtech.repository.SuggestSkillRepository;
import com.nashtech.service.dto.SuggestSkillDTO;
import com.nashtech.service.mapper.SuggestSkillMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link SuggestSkill}.
 */
@Service
@Transactional
public class SuggestSkillService {

    private final Logger log = LoggerFactory.getLogger(SuggestSkillService.class);

    private final SuggestSkillRepository suggestSkillRepository;

    private final SuggestSkillMapper suggestSkillMapper;

    public SuggestSkillService(SuggestSkillRepository suggestSkillRepository, SuggestSkillMapper suggestSkillMapper) {
        this.suggestSkillRepository = suggestSkillRepository;
        this.suggestSkillMapper = suggestSkillMapper;
    }

    /**
     * Save a suggestSkill.
     *
     * @param suggestSkillDTO the entity to save.
     * @return the persisted entity.
     */
    public SuggestSkillDTO save(SuggestSkillDTO suggestSkillDTO) {
        log.debug("Request to save SuggestSkill : {}", suggestSkillDTO);
        SuggestSkill suggestSkill = suggestSkillMapper.toEntity(suggestSkillDTO);
        suggestSkill = suggestSkillRepository.save(suggestSkill);
        return suggestSkillMapper.toDto(suggestSkill);
    }

    /**
     * Get all the suggestSkills.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SuggestSkillDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SuggestSkills");
        return suggestSkillRepository.findAll(pageable)
            .map(suggestSkillMapper::toDto);
    }


    /**
     * Get one suggestSkill by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SuggestSkillDTO> findOne(Long id) {
        log.debug("Request to get SuggestSkill : {}", id);
        return suggestSkillRepository.findById(id)
            .map(suggestSkillMapper::toDto);
    }

    /**
     * Delete the suggestSkill by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete SuggestSkill : {}", id);
        suggestSkillRepository.deleteById(id);
    }
}
