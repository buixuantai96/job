package com.nashtech.service;

import com.nashtech.domain.Expertise;
import com.nashtech.repository.ExpertiseRepository;
import com.nashtech.service.dto.ExpertiseDTO;
import com.nashtech.service.mapper.ExpertiseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Expertise}.
 */
@Service
@Transactional
public class ExpertiseService {

    private final Logger log = LoggerFactory.getLogger(ExpertiseService.class);

    private final ExpertiseRepository expertiseRepository;

    private final ExpertiseMapper expertiseMapper;

    public ExpertiseService(ExpertiseRepository expertiseRepository, ExpertiseMapper expertiseMapper) {
        this.expertiseRepository = expertiseRepository;
        this.expertiseMapper = expertiseMapper;
    }

    /**
     * Save a expertise.
     *
     * @param expertiseDTO the entity to save.
     * @return the persisted entity.
     */
    public ExpertiseDTO save(ExpertiseDTO expertiseDTO) {
        log.debug("Request to save Expertise : {}", expertiseDTO);
        Expertise expertise = expertiseMapper.toEntity(expertiseDTO);
        expertise = expertiseRepository.save(expertise);
        return expertiseMapper.toDto(expertise);
    }

    /**
     * Get all the expertise.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExpertiseDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Expertise");
        return expertiseRepository.findAll(pageable)
            .map(expertiseMapper::toDto);
    }


    /**
     * Get one expertise by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ExpertiseDTO> findOne(Long id) {
        log.debug("Request to get Expertise : {}", id);
        return expertiseRepository.findById(id)
            .map(expertiseMapper::toDto);
    }

    /**
     * Delete the expertise by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Expertise : {}", id);
        expertiseRepository.deleteById(id);
    }
}
