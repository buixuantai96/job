package com.nashtech.web.rest;

import com.nashtech.service.ExpertiseService;
import com.nashtech.web.rest.errors.BadRequestAlertException;
import com.nashtech.service.dto.ExpertiseDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.nashtech.domain.Expertise}.
 */
@RestController
@RequestMapping("/api")
public class ExpertiseResource {

    private final Logger log = LoggerFactory.getLogger(ExpertiseResource.class);

    private static final String ENTITY_NAME = "jobExpertise";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExpertiseService expertiseService;

    public ExpertiseResource(ExpertiseService expertiseService) {
        this.expertiseService = expertiseService;
    }

    /**
     * {@code POST  /expertise} : Create a new expertise.
     *
     * @param expertiseDTO the expertiseDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new expertiseDTO, or with status {@code 400 (Bad Request)} if the expertise has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/expertise")
    public ResponseEntity<ExpertiseDTO> createExpertise(@RequestBody ExpertiseDTO expertiseDTO) throws URISyntaxException {
        log.debug("REST request to save Expertise : {}", expertiseDTO);
        if (expertiseDTO.getId() != null) {
            throw new BadRequestAlertException("A new expertise cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExpertiseDTO result = expertiseService.save(expertiseDTO);
        return ResponseEntity.created(new URI("/api/expertise/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /expertise} : Updates an existing expertise.
     *
     * @param expertiseDTO the expertiseDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated expertiseDTO,
     * or with status {@code 400 (Bad Request)} if the expertiseDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the expertiseDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/expertise")
    public ResponseEntity<ExpertiseDTO> updateExpertise(@RequestBody ExpertiseDTO expertiseDTO) throws URISyntaxException {
        log.debug("REST request to update Expertise : {}", expertiseDTO);
        if (expertiseDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ExpertiseDTO result = expertiseService.save(expertiseDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, expertiseDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /expertise} : get all the expertise.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of expertise in body.
     */
    @GetMapping("/expertise")
    public ResponseEntity<List<ExpertiseDTO>> getAllExpertise(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Expertise");
        Page<ExpertiseDTO> page = expertiseService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /expertise/:id} : get the "id" expertise.
     *
     * @param id the id of the expertiseDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the expertiseDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/expertise/{id}")
    public ResponseEntity<ExpertiseDTO> getExpertise(@PathVariable Long id) {
        log.debug("REST request to get Expertise : {}", id);
        Optional<ExpertiseDTO> expertiseDTO = expertiseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(expertiseDTO);
    }

    /**
     * {@code DELETE  /expertise/:id} : delete the "id" expertise.
     *
     * @param id the id of the expertiseDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/expertise/{id}")
    public ResponseEntity<Void> deleteExpertise(@PathVariable Long id) {
        log.debug("REST request to delete Expertise : {}", id);
        expertiseService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
