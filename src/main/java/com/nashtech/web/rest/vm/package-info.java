/**
 * View Models used by Spring MVC REST controllers.
 */
package com.nashtech.web.rest.vm;
