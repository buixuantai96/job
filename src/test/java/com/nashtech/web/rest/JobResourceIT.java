package com.nashtech.web.rest;

import com.nashtech.JobApp;
import com.nashtech.domain.Job;
import com.nashtech.repository.JobRepository;
import com.nashtech.service.JobService;
import com.nashtech.service.dto.JobDTO;
import com.nashtech.service.mapper.JobMapper;
import com.nashtech.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.nashtech.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.nashtech.domain.enumeration.TermType;
import com.nashtech.domain.enumeration.TypeProject;
import com.nashtech.domain.enumeration.Viewer;
import com.nashtech.domain.enumeration.JobStatus;
import com.nashtech.domain.enumeration.Payment;
import com.nashtech.domain.enumeration.Level;
import com.nashtech.domain.enumeration.TimeProject;
import com.nashtech.domain.enumeration.TimeRequirement;
/**
 * Integration tests for the {@Link JobResource} REST controller.
 */
@SpringBootTest(classes = JobApp.class)
public class JobResourceIT {

    private static final Instant DEFAULT_CREATE_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATE_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_UPDATE_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATE_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_USER_CREATE_ID = 1L;
    private static final Long UPDATED_USER_CREATE_ID = 2L;

    private static final TermType DEFAULT_TERM_TYPE = TermType.LONG_TERM;
    private static final TermType UPDATED_TERM_TYPE = TermType.SHORT_TERM;

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final Long DEFAULT_CATEGORY_ITEM_ID = 1L;
    private static final Long UPDATED_CATEGORY_ITEM_ID = 2L;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    private static final TypeProject DEFAULT_TYPE_PROJECT = TypeProject.ONE_TIME;
    private static final TypeProject UPDATED_TYPE_PROJECT = TypeProject.ON_GOING;

    private static final Integer DEFAULT_NUM_OF_MEMBER = 1;
    private static final Integer UPDATED_NUM_OF_MEMBER = 2;

    private static final Boolean DEFAULT_COVER_LETTER = false;
    private static final Boolean UPDATED_COVER_LETTER = true;

    private static final Viewer DEFAULT_VIEWER = Viewer.ANYONE;
    private static final Viewer UPDATED_VIEWER = Viewer.ONLY_UPWORK_FREELANCER;

    private static final JobStatus DEFAULT_STATUS = JobStatus.DRAFT;
    private static final JobStatus UPDATED_STATUS = JobStatus.PENDING;

    private static final Payment DEFAULT_PAYMENT = Payment.PAY_BY_THE_HOUR;
    private static final Payment UPDATED_PAYMENT = Payment.PAY_A_FIXED_PRICE;

    private static final Level DEFAULT_LEVEL = Level.ENTRY;
    private static final Level UPDATED_LEVEL = Level.INTERMEDIATE;

    private static final TimeProject DEFAULT_TIME_PROJECT = TimeProject.MOTE_THAN_SIX_MONTHS;
    private static final TimeProject UPDATED_TIME_PROJECT = TimeProject.THREE_TO_SIX_MONTHS;

    private static final TimeRequirement DEFAULT_TIME_REQUIREMENT = TimeRequirement.MORE_THAN_30HOURS_PERWEEK;
    private static final TimeRequirement UPDATED_TIME_REQUIREMENT = TimeRequirement.LESS_THAN_30HOURS_PERWEEK;

    private static final Integer DEFAULT_SPECIFIC_BUDGET = 1;
    private static final Integer UPDATED_SPECIFIC_BUDGET = 2;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private JobMapper jobMapper;

    @Autowired
    private JobService jobService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restJobMockMvc;

    private Job job;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JobResource jobResource = new JobResource(jobService);
        this.restJobMockMvc = MockMvcBuilders.standaloneSetup(jobResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Job createEntity(EntityManager em) {
        Job job = new Job()
            .createAt(DEFAULT_CREATE_AT)
            .updateAt(DEFAULT_UPDATE_AT)
            .userCreateID(DEFAULT_USER_CREATE_ID)
            .termType(DEFAULT_TERM_TYPE)
            .title(DEFAULT_TITLE)
            .categoryItemID(DEFAULT_CATEGORY_ITEM_ID)
            .description(DEFAULT_DESCRIPTION)
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE)
            .typeProject(DEFAULT_TYPE_PROJECT)
            .numOfMember(DEFAULT_NUM_OF_MEMBER)
            .coverLetter(DEFAULT_COVER_LETTER)
            .viewer(DEFAULT_VIEWER)
            .status(DEFAULT_STATUS)
            .payment(DEFAULT_PAYMENT)
            .level(DEFAULT_LEVEL)
            .timeProject(DEFAULT_TIME_PROJECT)
            .timeRequirement(DEFAULT_TIME_REQUIREMENT)
            .specificBudget(DEFAULT_SPECIFIC_BUDGET);
        return job;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Job createUpdatedEntity(EntityManager em) {
        Job job = new Job()
            .createAt(UPDATED_CREATE_AT)
            .updateAt(UPDATED_UPDATE_AT)
            .userCreateID(UPDATED_USER_CREATE_ID)
            .termType(UPDATED_TERM_TYPE)
            .title(UPDATED_TITLE)
            .categoryItemID(UPDATED_CATEGORY_ITEM_ID)
            .description(UPDATED_DESCRIPTION)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .typeProject(UPDATED_TYPE_PROJECT)
            .numOfMember(UPDATED_NUM_OF_MEMBER)
            .coverLetter(UPDATED_COVER_LETTER)
            .viewer(UPDATED_VIEWER)
            .status(UPDATED_STATUS)
            .payment(UPDATED_PAYMENT)
            .level(UPDATED_LEVEL)
            .timeProject(UPDATED_TIME_PROJECT)
            .timeRequirement(UPDATED_TIME_REQUIREMENT)
            .specificBudget(UPDATED_SPECIFIC_BUDGET);
        return job;
    }

    @BeforeEach
    public void initTest() {
        job = createEntity(em);
    }

    @Test
    @Transactional
    public void createJob() throws Exception {
        int databaseSizeBeforeCreate = jobRepository.findAll().size();

        // Create the Job
        JobDTO jobDTO = jobMapper.toDto(job);
        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isCreated());

        // Validate the Job in the database
        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeCreate + 1);
        Job testJob = jobList.get(jobList.size() - 1);
        assertThat(testJob.getCreateAt()).isEqualTo(DEFAULT_CREATE_AT);
        assertThat(testJob.getUpdateAt()).isEqualTo(DEFAULT_UPDATE_AT);
        assertThat(testJob.getUserCreateID()).isEqualTo(DEFAULT_USER_CREATE_ID);
        assertThat(testJob.getTermType()).isEqualTo(DEFAULT_TERM_TYPE);
        assertThat(testJob.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testJob.getCategoryItemID()).isEqualTo(DEFAULT_CATEGORY_ITEM_ID);
        assertThat(testJob.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testJob.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testJob.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
        assertThat(testJob.getTypeProject()).isEqualTo(DEFAULT_TYPE_PROJECT);
        assertThat(testJob.getNumOfMember()).isEqualTo(DEFAULT_NUM_OF_MEMBER);
        assertThat(testJob.isCoverLetter()).isEqualTo(DEFAULT_COVER_LETTER);
        assertThat(testJob.getViewer()).isEqualTo(DEFAULT_VIEWER);
        assertThat(testJob.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testJob.getPayment()).isEqualTo(DEFAULT_PAYMENT);
        assertThat(testJob.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testJob.getTimeProject()).isEqualTo(DEFAULT_TIME_PROJECT);
        assertThat(testJob.getTimeRequirement()).isEqualTo(DEFAULT_TIME_REQUIREMENT);
        assertThat(testJob.getSpecificBudget()).isEqualTo(DEFAULT_SPECIFIC_BUDGET);
    }

    @Test
    @Transactional
    public void createJobWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jobRepository.findAll().size();

        // Create the Job with an existing ID
        job.setId(1L);
        JobDTO jobDTO = jobMapper.toDto(job);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Job in the database
        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCreateAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setCreateAt(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUpdateAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setUpdateAt(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUserCreateIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setUserCreateID(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTermTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setTermType(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setTitle(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCategoryItemIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setCategoryItemID(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setDescription(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeProjectIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setTypeProject(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumOfMemberIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setNumOfMember(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkViewerIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setViewer(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPaymentIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setPayment(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLevelIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setLevel(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllJobs() throws Exception {
        // Initialize the database
        jobRepository.saveAndFlush(job);

        // Get all the jobList
        restJobMockMvc.perform(get("/api/jobs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(job.getId().intValue())))
            .andExpect(jsonPath("$.[*].createAt").value(hasItem(DEFAULT_CREATE_AT.toString())))
            .andExpect(jsonPath("$.[*].updateAt").value(hasItem(DEFAULT_UPDATE_AT.toString())))
            .andExpect(jsonPath("$.[*].userCreateID").value(hasItem(DEFAULT_USER_CREATE_ID.intValue())))
            .andExpect(jsonPath("$.[*].termType").value(hasItem(DEFAULT_TERM_TYPE.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].categoryItemID").value(hasItem(DEFAULT_CATEGORY_ITEM_ID.intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))))
            .andExpect(jsonPath("$.[*].typeProject").value(hasItem(DEFAULT_TYPE_PROJECT.toString())))
            .andExpect(jsonPath("$.[*].numOfMember").value(hasItem(DEFAULT_NUM_OF_MEMBER)))
            .andExpect(jsonPath("$.[*].coverLetter").value(hasItem(DEFAULT_COVER_LETTER.booleanValue())))
            .andExpect(jsonPath("$.[*].viewer").value(hasItem(DEFAULT_VIEWER.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].payment").value(hasItem(DEFAULT_PAYMENT.toString())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL.toString())))
            .andExpect(jsonPath("$.[*].timeProject").value(hasItem(DEFAULT_TIME_PROJECT.toString())))
            .andExpect(jsonPath("$.[*].timeRequirement").value(hasItem(DEFAULT_TIME_REQUIREMENT.toString())))
            .andExpect(jsonPath("$.[*].specificBudget").value(hasItem(DEFAULT_SPECIFIC_BUDGET)));
    }
    
    @Test
    @Transactional
    public void getJob() throws Exception {
        // Initialize the database
        jobRepository.saveAndFlush(job);

        // Get the job
        restJobMockMvc.perform(get("/api/jobs/{id}", job.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(job.getId().intValue()))
            .andExpect(jsonPath("$.createAt").value(DEFAULT_CREATE_AT.toString()))
            .andExpect(jsonPath("$.updateAt").value(DEFAULT_UPDATE_AT.toString()))
            .andExpect(jsonPath("$.userCreateID").value(DEFAULT_USER_CREATE_ID.intValue()))
            .andExpect(jsonPath("$.termType").value(DEFAULT_TERM_TYPE.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.categoryItemID").value(DEFAULT_CATEGORY_ITEM_ID.intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64Utils.encodeToString(DEFAULT_IMAGE)))
            .andExpect(jsonPath("$.typeProject").value(DEFAULT_TYPE_PROJECT.toString()))
            .andExpect(jsonPath("$.numOfMember").value(DEFAULT_NUM_OF_MEMBER))
            .andExpect(jsonPath("$.coverLetter").value(DEFAULT_COVER_LETTER.booleanValue()))
            .andExpect(jsonPath("$.viewer").value(DEFAULT_VIEWER.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.payment").value(DEFAULT_PAYMENT.toString()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL.toString()))
            .andExpect(jsonPath("$.timeProject").value(DEFAULT_TIME_PROJECT.toString()))
            .andExpect(jsonPath("$.timeRequirement").value(DEFAULT_TIME_REQUIREMENT.toString()))
            .andExpect(jsonPath("$.specificBudget").value(DEFAULT_SPECIFIC_BUDGET));
    }

    @Test
    @Transactional
    public void getNonExistingJob() throws Exception {
        // Get the job
        restJobMockMvc.perform(get("/api/jobs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJob() throws Exception {
        // Initialize the database
        jobRepository.saveAndFlush(job);

        int databaseSizeBeforeUpdate = jobRepository.findAll().size();

        // Update the job
        Job updatedJob = jobRepository.findById(job.getId()).get();
        // Disconnect from session so that the updates on updatedJob are not directly saved in db
        em.detach(updatedJob);
        updatedJob
            .createAt(UPDATED_CREATE_AT)
            .updateAt(UPDATED_UPDATE_AT)
            .userCreateID(UPDATED_USER_CREATE_ID)
            .termType(UPDATED_TERM_TYPE)
            .title(UPDATED_TITLE)
            .categoryItemID(UPDATED_CATEGORY_ITEM_ID)
            .description(UPDATED_DESCRIPTION)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .typeProject(UPDATED_TYPE_PROJECT)
            .numOfMember(UPDATED_NUM_OF_MEMBER)
            .coverLetter(UPDATED_COVER_LETTER)
            .viewer(UPDATED_VIEWER)
            .status(UPDATED_STATUS)
            .payment(UPDATED_PAYMENT)
            .level(UPDATED_LEVEL)
            .timeProject(UPDATED_TIME_PROJECT)
            .timeRequirement(UPDATED_TIME_REQUIREMENT)
            .specificBudget(UPDATED_SPECIFIC_BUDGET);
        JobDTO jobDTO = jobMapper.toDto(updatedJob);

        restJobMockMvc.perform(put("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isOk());

        // Validate the Job in the database
        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeUpdate);
        Job testJob = jobList.get(jobList.size() - 1);
        assertThat(testJob.getCreateAt()).isEqualTo(UPDATED_CREATE_AT);
        assertThat(testJob.getUpdateAt()).isEqualTo(UPDATED_UPDATE_AT);
        assertThat(testJob.getUserCreateID()).isEqualTo(UPDATED_USER_CREATE_ID);
        assertThat(testJob.getTermType()).isEqualTo(UPDATED_TERM_TYPE);
        assertThat(testJob.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testJob.getCategoryItemID()).isEqualTo(UPDATED_CATEGORY_ITEM_ID);
        assertThat(testJob.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testJob.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testJob.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
        assertThat(testJob.getTypeProject()).isEqualTo(UPDATED_TYPE_PROJECT);
        assertThat(testJob.getNumOfMember()).isEqualTo(UPDATED_NUM_OF_MEMBER);
        assertThat(testJob.isCoverLetter()).isEqualTo(UPDATED_COVER_LETTER);
        assertThat(testJob.getViewer()).isEqualTo(UPDATED_VIEWER);
        assertThat(testJob.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testJob.getPayment()).isEqualTo(UPDATED_PAYMENT);
        assertThat(testJob.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testJob.getTimeProject()).isEqualTo(UPDATED_TIME_PROJECT);
        assertThat(testJob.getTimeRequirement()).isEqualTo(UPDATED_TIME_REQUIREMENT);
        assertThat(testJob.getSpecificBudget()).isEqualTo(UPDATED_SPECIFIC_BUDGET);
    }

    @Test
    @Transactional
    public void updateNonExistingJob() throws Exception {
        int databaseSizeBeforeUpdate = jobRepository.findAll().size();

        // Create the Job
        JobDTO jobDTO = jobMapper.toDto(job);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJobMockMvc.perform(put("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Job in the database
        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteJob() throws Exception {
        // Initialize the database
        jobRepository.saveAndFlush(job);

        int databaseSizeBeforeDelete = jobRepository.findAll().size();

        // Delete the job
        restJobMockMvc.perform(delete("/api/jobs/{id}", job.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Job.class);
        Job job1 = new Job();
        job1.setId(1L);
        Job job2 = new Job();
        job2.setId(job1.getId());
        assertThat(job1).isEqualTo(job2);
        job2.setId(2L);
        assertThat(job1).isNotEqualTo(job2);
        job1.setId(null);
        assertThat(job1).isNotEqualTo(job2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(JobDTO.class);
        JobDTO jobDTO1 = new JobDTO();
        jobDTO1.setId(1L);
        JobDTO jobDTO2 = new JobDTO();
        assertThat(jobDTO1).isNotEqualTo(jobDTO2);
        jobDTO2.setId(jobDTO1.getId());
        assertThat(jobDTO1).isEqualTo(jobDTO2);
        jobDTO2.setId(2L);
        assertThat(jobDTO1).isNotEqualTo(jobDTO2);
        jobDTO1.setId(null);
        assertThat(jobDTO1).isNotEqualTo(jobDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(jobMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(jobMapper.fromId(null)).isNull();
    }
}
