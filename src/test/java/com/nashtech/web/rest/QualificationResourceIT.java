package com.nashtech.web.rest;

import com.nashtech.JobApp;
import com.nashtech.domain.Qualification;
import com.nashtech.repository.QualificationRepository;
import com.nashtech.service.QualificationService;
import com.nashtech.service.dto.QualificationDTO;
import com.nashtech.service.mapper.QualificationMapper;
import com.nashtech.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.nashtech.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.nashtech.domain.enumeration.FreelancerType;
import com.nashtech.domain.enumeration.HoursBilledonUpwork;
import com.nashtech.domain.enumeration.JobSuccessScore;
import com.nashtech.domain.enumeration.RisingTalent;
import com.nashtech.domain.enumeration.Location;
import com.nashtech.domain.enumeration.EnglishLevel;
import com.nashtech.domain.enumeration.Group;
/**
 * Integration tests for the {@Link QualificationResource} REST controller.
 */
@SpringBootTest(classes = JobApp.class)
public class QualificationResourceIT {

    private static final FreelancerType DEFAULT_FREELANCERTYPE = FreelancerType.NO_PREFER;
    private static final FreelancerType UPDATED_FREELANCERTYPE = FreelancerType.INDEPENDENT;

    private static final HoursBilledonUpwork DEFAULT_HOURSBILLEDONUPWORK = HoursBilledonUpwork.ANY;
    private static final HoursBilledonUpwork UPDATED_HOURSBILLEDONUPWORK = HoursBilledonUpwork.ONE;

    private static final JobSuccessScore DEFAULT_JOBSUCCESSSCORE = JobSuccessScore.ANY;
    private static final JobSuccessScore UPDATED_JOBSUCCESSSCORE = JobSuccessScore.NINETY_PERCENT;

    private static final RisingTalent DEFAULT_RISINGTALENT = RisingTalent.INCLUDE;
    private static final RisingTalent UPDATED_RISINGTALENT = RisingTalent.DONT;

    private static final Location DEFAULT_LOCATION = Location.ANY;
    private static final Location UPDATED_LOCATION = Location.AFRICA;

    private static final EnglishLevel DEFAULT_ENGLISHLEVEL = EnglishLevel.ANY;
    private static final EnglishLevel UPDATED_ENGLISHLEVEL = EnglishLevel.BASIC;

    private static final Group DEFAULT_GROUP = Group.NO_PREFERENT;
    private static final Group UPDATED_GROUP = Group.AWEBER_EMAIL_MARTKETING;

    @Autowired
    private QualificationRepository qualificationRepository;

    @Autowired
    private QualificationMapper qualificationMapper;

    @Autowired
    private QualificationService qualificationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restQualificationMockMvc;

    private Qualification qualification;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final QualificationResource qualificationResource = new QualificationResource(qualificationService);
        this.restQualificationMockMvc = MockMvcBuilders.standaloneSetup(qualificationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Qualification createEntity(EntityManager em) {
        Qualification qualification = new Qualification()
            .freelancertype(DEFAULT_FREELANCERTYPE)
            .hoursbilledonupwork(DEFAULT_HOURSBILLEDONUPWORK)
            .jobsuccessscore(DEFAULT_JOBSUCCESSSCORE)
            .risingtalent(DEFAULT_RISINGTALENT)
            .location(DEFAULT_LOCATION)
            .englishlevel(DEFAULT_ENGLISHLEVEL)
            .group(DEFAULT_GROUP);
        return qualification;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Qualification createUpdatedEntity(EntityManager em) {
        Qualification qualification = new Qualification()
            .freelancertype(UPDATED_FREELANCERTYPE)
            .hoursbilledonupwork(UPDATED_HOURSBILLEDONUPWORK)
            .jobsuccessscore(UPDATED_JOBSUCCESSSCORE)
            .risingtalent(UPDATED_RISINGTALENT)
            .location(UPDATED_LOCATION)
            .englishlevel(UPDATED_ENGLISHLEVEL)
            .group(UPDATED_GROUP);
        return qualification;
    }

    @BeforeEach
    public void initTest() {
        qualification = createEntity(em);
    }

    @Test
    @Transactional
    public void createQualification() throws Exception {
        int databaseSizeBeforeCreate = qualificationRepository.findAll().size();

        // Create the Qualification
        QualificationDTO qualificationDTO = qualificationMapper.toDto(qualification);
        restQualificationMockMvc.perform(post("/api/qualifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qualificationDTO)))
            .andExpect(status().isCreated());

        // Validate the Qualification in the database
        List<Qualification> qualificationList = qualificationRepository.findAll();
        assertThat(qualificationList).hasSize(databaseSizeBeforeCreate + 1);
        Qualification testQualification = qualificationList.get(qualificationList.size() - 1);
        assertThat(testQualification.getFreelancertype()).isEqualTo(DEFAULT_FREELANCERTYPE);
        assertThat(testQualification.getHoursbilledonupwork()).isEqualTo(DEFAULT_HOURSBILLEDONUPWORK);
        assertThat(testQualification.getJobsuccessscore()).isEqualTo(DEFAULT_JOBSUCCESSSCORE);
        assertThat(testQualification.getRisingtalent()).isEqualTo(DEFAULT_RISINGTALENT);
        assertThat(testQualification.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testQualification.getEnglishlevel()).isEqualTo(DEFAULT_ENGLISHLEVEL);
        assertThat(testQualification.getGroup()).isEqualTo(DEFAULT_GROUP);
    }

    @Test
    @Transactional
    public void createQualificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = qualificationRepository.findAll().size();

        // Create the Qualification with an existing ID
        qualification.setId(1L);
        QualificationDTO qualificationDTO = qualificationMapper.toDto(qualification);

        // An entity with an existing ID cannot be created, so this API call must fail
        restQualificationMockMvc.perform(post("/api/qualifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qualificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Qualification in the database
        List<Qualification> qualificationList = qualificationRepository.findAll();
        assertThat(qualificationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkFreelancertypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = qualificationRepository.findAll().size();
        // set the field null
        qualification.setFreelancertype(null);

        // Create the Qualification, which fails.
        QualificationDTO qualificationDTO = qualificationMapper.toDto(qualification);

        restQualificationMockMvc.perform(post("/api/qualifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qualificationDTO)))
            .andExpect(status().isBadRequest());

        List<Qualification> qualificationList = qualificationRepository.findAll();
        assertThat(qualificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHoursbilledonupworkIsRequired() throws Exception {
        int databaseSizeBeforeTest = qualificationRepository.findAll().size();
        // set the field null
        qualification.setHoursbilledonupwork(null);

        // Create the Qualification, which fails.
        QualificationDTO qualificationDTO = qualificationMapper.toDto(qualification);

        restQualificationMockMvc.perform(post("/api/qualifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qualificationDTO)))
            .andExpect(status().isBadRequest());

        List<Qualification> qualificationList = qualificationRepository.findAll();
        assertThat(qualificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkJobsuccessscoreIsRequired() throws Exception {
        int databaseSizeBeforeTest = qualificationRepository.findAll().size();
        // set the field null
        qualification.setJobsuccessscore(null);

        // Create the Qualification, which fails.
        QualificationDTO qualificationDTO = qualificationMapper.toDto(qualification);

        restQualificationMockMvc.perform(post("/api/qualifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qualificationDTO)))
            .andExpect(status().isBadRequest());

        List<Qualification> qualificationList = qualificationRepository.findAll();
        assertThat(qualificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRisingtalentIsRequired() throws Exception {
        int databaseSizeBeforeTest = qualificationRepository.findAll().size();
        // set the field null
        qualification.setRisingtalent(null);

        // Create the Qualification, which fails.
        QualificationDTO qualificationDTO = qualificationMapper.toDto(qualification);

        restQualificationMockMvc.perform(post("/api/qualifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qualificationDTO)))
            .andExpect(status().isBadRequest());

        List<Qualification> qualificationList = qualificationRepository.findAll();
        assertThat(qualificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLocationIsRequired() throws Exception {
        int databaseSizeBeforeTest = qualificationRepository.findAll().size();
        // set the field null
        qualification.setLocation(null);

        // Create the Qualification, which fails.
        QualificationDTO qualificationDTO = qualificationMapper.toDto(qualification);

        restQualificationMockMvc.perform(post("/api/qualifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qualificationDTO)))
            .andExpect(status().isBadRequest());

        List<Qualification> qualificationList = qualificationRepository.findAll();
        assertThat(qualificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnglishlevelIsRequired() throws Exception {
        int databaseSizeBeforeTest = qualificationRepository.findAll().size();
        // set the field null
        qualification.setEnglishlevel(null);

        // Create the Qualification, which fails.
        QualificationDTO qualificationDTO = qualificationMapper.toDto(qualification);

        restQualificationMockMvc.perform(post("/api/qualifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qualificationDTO)))
            .andExpect(status().isBadRequest());

        List<Qualification> qualificationList = qualificationRepository.findAll();
        assertThat(qualificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGroupIsRequired() throws Exception {
        int databaseSizeBeforeTest = qualificationRepository.findAll().size();
        // set the field null
        qualification.setGroup(null);

        // Create the Qualification, which fails.
        QualificationDTO qualificationDTO = qualificationMapper.toDto(qualification);

        restQualificationMockMvc.perform(post("/api/qualifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qualificationDTO)))
            .andExpect(status().isBadRequest());

        List<Qualification> qualificationList = qualificationRepository.findAll();
        assertThat(qualificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllQualifications() throws Exception {
        // Initialize the database
        qualificationRepository.saveAndFlush(qualification);

        // Get all the qualificationList
        restQualificationMockMvc.perform(get("/api/qualifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(qualification.getId().intValue())))
            .andExpect(jsonPath("$.[*].freelancertype").value(hasItem(DEFAULT_FREELANCERTYPE.toString())))
            .andExpect(jsonPath("$.[*].hoursbilledonupwork").value(hasItem(DEFAULT_HOURSBILLEDONUPWORK.toString())))
            .andExpect(jsonPath("$.[*].jobsuccessscore").value(hasItem(DEFAULT_JOBSUCCESSSCORE.toString())))
            .andExpect(jsonPath("$.[*].risingtalent").value(hasItem(DEFAULT_RISINGTALENT.toString())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].englishlevel").value(hasItem(DEFAULT_ENGLISHLEVEL.toString())))
            .andExpect(jsonPath("$.[*].group").value(hasItem(DEFAULT_GROUP.toString())));
    }
    
    @Test
    @Transactional
    public void getQualification() throws Exception {
        // Initialize the database
        qualificationRepository.saveAndFlush(qualification);

        // Get the qualification
        restQualificationMockMvc.perform(get("/api/qualifications/{id}", qualification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(qualification.getId().intValue()))
            .andExpect(jsonPath("$.freelancertype").value(DEFAULT_FREELANCERTYPE.toString()))
            .andExpect(jsonPath("$.hoursbilledonupwork").value(DEFAULT_HOURSBILLEDONUPWORK.toString()))
            .andExpect(jsonPath("$.jobsuccessscore").value(DEFAULT_JOBSUCCESSSCORE.toString()))
            .andExpect(jsonPath("$.risingtalent").value(DEFAULT_RISINGTALENT.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.englishlevel").value(DEFAULT_ENGLISHLEVEL.toString()))
            .andExpect(jsonPath("$.group").value(DEFAULT_GROUP.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingQualification() throws Exception {
        // Get the qualification
        restQualificationMockMvc.perform(get("/api/qualifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateQualification() throws Exception {
        // Initialize the database
        qualificationRepository.saveAndFlush(qualification);

        int databaseSizeBeforeUpdate = qualificationRepository.findAll().size();

        // Update the qualification
        Qualification updatedQualification = qualificationRepository.findById(qualification.getId()).get();
        // Disconnect from session so that the updates on updatedQualification are not directly saved in db
        em.detach(updatedQualification);
        updatedQualification
            .freelancertype(UPDATED_FREELANCERTYPE)
            .hoursbilledonupwork(UPDATED_HOURSBILLEDONUPWORK)
            .jobsuccessscore(UPDATED_JOBSUCCESSSCORE)
            .risingtalent(UPDATED_RISINGTALENT)
            .location(UPDATED_LOCATION)
            .englishlevel(UPDATED_ENGLISHLEVEL)
            .group(UPDATED_GROUP);
        QualificationDTO qualificationDTO = qualificationMapper.toDto(updatedQualification);

        restQualificationMockMvc.perform(put("/api/qualifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qualificationDTO)))
            .andExpect(status().isOk());

        // Validate the Qualification in the database
        List<Qualification> qualificationList = qualificationRepository.findAll();
        assertThat(qualificationList).hasSize(databaseSizeBeforeUpdate);
        Qualification testQualification = qualificationList.get(qualificationList.size() - 1);
        assertThat(testQualification.getFreelancertype()).isEqualTo(UPDATED_FREELANCERTYPE);
        assertThat(testQualification.getHoursbilledonupwork()).isEqualTo(UPDATED_HOURSBILLEDONUPWORK);
        assertThat(testQualification.getJobsuccessscore()).isEqualTo(UPDATED_JOBSUCCESSSCORE);
        assertThat(testQualification.getRisingtalent()).isEqualTo(UPDATED_RISINGTALENT);
        assertThat(testQualification.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testQualification.getEnglishlevel()).isEqualTo(UPDATED_ENGLISHLEVEL);
        assertThat(testQualification.getGroup()).isEqualTo(UPDATED_GROUP);
    }

    @Test
    @Transactional
    public void updateNonExistingQualification() throws Exception {
        int databaseSizeBeforeUpdate = qualificationRepository.findAll().size();

        // Create the Qualification
        QualificationDTO qualificationDTO = qualificationMapper.toDto(qualification);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restQualificationMockMvc.perform(put("/api/qualifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qualificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Qualification in the database
        List<Qualification> qualificationList = qualificationRepository.findAll();
        assertThat(qualificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteQualification() throws Exception {
        // Initialize the database
        qualificationRepository.saveAndFlush(qualification);

        int databaseSizeBeforeDelete = qualificationRepository.findAll().size();

        // Delete the qualification
        restQualificationMockMvc.perform(delete("/api/qualifications/{id}", qualification.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Qualification> qualificationList = qualificationRepository.findAll();
        assertThat(qualificationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Qualification.class);
        Qualification qualification1 = new Qualification();
        qualification1.setId(1L);
        Qualification qualification2 = new Qualification();
        qualification2.setId(qualification1.getId());
        assertThat(qualification1).isEqualTo(qualification2);
        qualification2.setId(2L);
        assertThat(qualification1).isNotEqualTo(qualification2);
        qualification1.setId(null);
        assertThat(qualification1).isNotEqualTo(qualification2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(QualificationDTO.class);
        QualificationDTO qualificationDTO1 = new QualificationDTO();
        qualificationDTO1.setId(1L);
        QualificationDTO qualificationDTO2 = new QualificationDTO();
        assertThat(qualificationDTO1).isNotEqualTo(qualificationDTO2);
        qualificationDTO2.setId(qualificationDTO1.getId());
        assertThat(qualificationDTO1).isEqualTo(qualificationDTO2);
        qualificationDTO2.setId(2L);
        assertThat(qualificationDTO1).isNotEqualTo(qualificationDTO2);
        qualificationDTO1.setId(null);
        assertThat(qualificationDTO1).isNotEqualTo(qualificationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(qualificationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(qualificationMapper.fromId(null)).isNull();
    }
}
