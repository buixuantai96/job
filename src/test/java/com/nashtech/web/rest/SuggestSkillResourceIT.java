package com.nashtech.web.rest;

import com.nashtech.JobApp;
import com.nashtech.domain.SuggestSkill;
import com.nashtech.repository.SuggestSkillRepository;
import com.nashtech.service.SuggestSkillService;
import com.nashtech.service.dto.SuggestSkillDTO;
import com.nashtech.service.mapper.SuggestSkillMapper;
import com.nashtech.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.nashtech.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.nashtech.domain.enumeration.Status;
/**
 * Integration tests for the {@Link SuggestSkillResource} REST controller.
 */
@SpringBootTest(classes = JobApp.class)
public class SuggestSkillResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_LINK = "AAAAAAAAAA";
    private static final String UPDATED_LINK = "BBBBBBBBBB";

    private static final Status DEFAULT_STATUS = Status.PENDING;
    private static final Status UPDATED_STATUS = Status.ACCEPT;

    @Autowired
    private SuggestSkillRepository suggestSkillRepository;

    @Autowired
    private SuggestSkillMapper suggestSkillMapper;

    @Autowired
    private SuggestSkillService suggestSkillService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSuggestSkillMockMvc;

    private SuggestSkill suggestSkill;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SuggestSkillResource suggestSkillResource = new SuggestSkillResource(suggestSkillService);
        this.restSuggestSkillMockMvc = MockMvcBuilders.standaloneSetup(suggestSkillResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SuggestSkill createEntity(EntityManager em) {
        SuggestSkill suggestSkill = new SuggestSkill()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .link(DEFAULT_LINK)
            .status(DEFAULT_STATUS);
        return suggestSkill;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SuggestSkill createUpdatedEntity(EntityManager em) {
        SuggestSkill suggestSkill = new SuggestSkill()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .link(UPDATED_LINK)
            .status(UPDATED_STATUS);
        return suggestSkill;
    }

    @BeforeEach
    public void initTest() {
        suggestSkill = createEntity(em);
    }

    @Test
    @Transactional
    public void createSuggestSkill() throws Exception {
        int databaseSizeBeforeCreate = suggestSkillRepository.findAll().size();

        // Create the SuggestSkill
        SuggestSkillDTO suggestSkillDTO = suggestSkillMapper.toDto(suggestSkill);
        restSuggestSkillMockMvc.perform(post("/api/suggest-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(suggestSkillDTO)))
            .andExpect(status().isCreated());

        // Validate the SuggestSkill in the database
        List<SuggestSkill> suggestSkillList = suggestSkillRepository.findAll();
        assertThat(suggestSkillList).hasSize(databaseSizeBeforeCreate + 1);
        SuggestSkill testSuggestSkill = suggestSkillList.get(suggestSkillList.size() - 1);
        assertThat(testSuggestSkill.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSuggestSkill.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSuggestSkill.getLink()).isEqualTo(DEFAULT_LINK);
        assertThat(testSuggestSkill.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createSuggestSkillWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = suggestSkillRepository.findAll().size();

        // Create the SuggestSkill with an existing ID
        suggestSkill.setId(1L);
        SuggestSkillDTO suggestSkillDTO = suggestSkillMapper.toDto(suggestSkill);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSuggestSkillMockMvc.perform(post("/api/suggest-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(suggestSkillDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SuggestSkill in the database
        List<SuggestSkill> suggestSkillList = suggestSkillRepository.findAll();
        assertThat(suggestSkillList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = suggestSkillRepository.findAll().size();
        // set the field null
        suggestSkill.setName(null);

        // Create the SuggestSkill, which fails.
        SuggestSkillDTO suggestSkillDTO = suggestSkillMapper.toDto(suggestSkill);

        restSuggestSkillMockMvc.perform(post("/api/suggest-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(suggestSkillDTO)))
            .andExpect(status().isBadRequest());

        List<SuggestSkill> suggestSkillList = suggestSkillRepository.findAll();
        assertThat(suggestSkillList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSuggestSkills() throws Exception {
        // Initialize the database
        suggestSkillRepository.saveAndFlush(suggestSkill);

        // Get all the suggestSkillList
        restSuggestSkillMockMvc.perform(get("/api/suggest-skills?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(suggestSkill.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].link").value(hasItem(DEFAULT_LINK.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }
    
    @Test
    @Transactional
    public void getSuggestSkill() throws Exception {
        // Initialize the database
        suggestSkillRepository.saveAndFlush(suggestSkill);

        // Get the suggestSkill
        restSuggestSkillMockMvc.perform(get("/api/suggest-skills/{id}", suggestSkill.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(suggestSkill.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.link").value(DEFAULT_LINK.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSuggestSkill() throws Exception {
        // Get the suggestSkill
        restSuggestSkillMockMvc.perform(get("/api/suggest-skills/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSuggestSkill() throws Exception {
        // Initialize the database
        suggestSkillRepository.saveAndFlush(suggestSkill);

        int databaseSizeBeforeUpdate = suggestSkillRepository.findAll().size();

        // Update the suggestSkill
        SuggestSkill updatedSuggestSkill = suggestSkillRepository.findById(suggestSkill.getId()).get();
        // Disconnect from session so that the updates on updatedSuggestSkill are not directly saved in db
        em.detach(updatedSuggestSkill);
        updatedSuggestSkill
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .link(UPDATED_LINK)
            .status(UPDATED_STATUS);
        SuggestSkillDTO suggestSkillDTO = suggestSkillMapper.toDto(updatedSuggestSkill);

        restSuggestSkillMockMvc.perform(put("/api/suggest-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(suggestSkillDTO)))
            .andExpect(status().isOk());

        // Validate the SuggestSkill in the database
        List<SuggestSkill> suggestSkillList = suggestSkillRepository.findAll();
        assertThat(suggestSkillList).hasSize(databaseSizeBeforeUpdate);
        SuggestSkill testSuggestSkill = suggestSkillList.get(suggestSkillList.size() - 1);
        assertThat(testSuggestSkill.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSuggestSkill.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSuggestSkill.getLink()).isEqualTo(UPDATED_LINK);
        assertThat(testSuggestSkill.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingSuggestSkill() throws Exception {
        int databaseSizeBeforeUpdate = suggestSkillRepository.findAll().size();

        // Create the SuggestSkill
        SuggestSkillDTO suggestSkillDTO = suggestSkillMapper.toDto(suggestSkill);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSuggestSkillMockMvc.perform(put("/api/suggest-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(suggestSkillDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SuggestSkill in the database
        List<SuggestSkill> suggestSkillList = suggestSkillRepository.findAll();
        assertThat(suggestSkillList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSuggestSkill() throws Exception {
        // Initialize the database
        suggestSkillRepository.saveAndFlush(suggestSkill);

        int databaseSizeBeforeDelete = suggestSkillRepository.findAll().size();

        // Delete the suggestSkill
        restSuggestSkillMockMvc.perform(delete("/api/suggest-skills/{id}", suggestSkill.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<SuggestSkill> suggestSkillList = suggestSkillRepository.findAll();
        assertThat(suggestSkillList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SuggestSkill.class);
        SuggestSkill suggestSkill1 = new SuggestSkill();
        suggestSkill1.setId(1L);
        SuggestSkill suggestSkill2 = new SuggestSkill();
        suggestSkill2.setId(suggestSkill1.getId());
        assertThat(suggestSkill1).isEqualTo(suggestSkill2);
        suggestSkill2.setId(2L);
        assertThat(suggestSkill1).isNotEqualTo(suggestSkill2);
        suggestSkill1.setId(null);
        assertThat(suggestSkill1).isNotEqualTo(suggestSkill2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SuggestSkillDTO.class);
        SuggestSkillDTO suggestSkillDTO1 = new SuggestSkillDTO();
        suggestSkillDTO1.setId(1L);
        SuggestSkillDTO suggestSkillDTO2 = new SuggestSkillDTO();
        assertThat(suggestSkillDTO1).isNotEqualTo(suggestSkillDTO2);
        suggestSkillDTO2.setId(suggestSkillDTO1.getId());
        assertThat(suggestSkillDTO1).isEqualTo(suggestSkillDTO2);
        suggestSkillDTO2.setId(2L);
        assertThat(suggestSkillDTO1).isNotEqualTo(suggestSkillDTO2);
        suggestSkillDTO1.setId(null);
        assertThat(suggestSkillDTO1).isNotEqualTo(suggestSkillDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(suggestSkillMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(suggestSkillMapper.fromId(null)).isNull();
    }
}
