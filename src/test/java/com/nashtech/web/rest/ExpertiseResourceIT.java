package com.nashtech.web.rest;

import com.nashtech.JobApp;
import com.nashtech.domain.Expertise;
import com.nashtech.repository.ExpertiseRepository;
import com.nashtech.service.ExpertiseService;
import com.nashtech.service.dto.ExpertiseDTO;
import com.nashtech.service.mapper.ExpertiseMapper;
import com.nashtech.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.nashtech.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ExpertiseResource} REST controller.
 */
@SpringBootTest(classes = JobApp.class)
public class ExpertiseResourceIT {

    private static final String DEFAULT_SKILL = "AAAAAAAAAA";
    private static final String UPDATED_SKILL = "BBBBBBBBBB";

    private static final String DEFAULT_EXPERTISE = "AAAAAAAAAA";
    private static final String UPDATED_EXPERTISE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDITIONAL = "AAAAAAAAAA";
    private static final String UPDATED_ADDITIONAL = "BBBBBBBBBB";

    @Autowired
    private ExpertiseRepository expertiseRepository;

    @Autowired
    private ExpertiseMapper expertiseMapper;

    @Autowired
    private ExpertiseService expertiseService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restExpertiseMockMvc;

    private Expertise expertise;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ExpertiseResource expertiseResource = new ExpertiseResource(expertiseService);
        this.restExpertiseMockMvc = MockMvcBuilders.standaloneSetup(expertiseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Expertise createEntity(EntityManager em) {
        Expertise expertise = new Expertise()
            .skill(DEFAULT_SKILL)
            .expertise(DEFAULT_EXPERTISE)
            .additional(DEFAULT_ADDITIONAL);
        return expertise;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Expertise createUpdatedEntity(EntityManager em) {
        Expertise expertise = new Expertise()
            .skill(UPDATED_SKILL)
            .expertise(UPDATED_EXPERTISE)
            .additional(UPDATED_ADDITIONAL);
        return expertise;
    }

    @BeforeEach
    public void initTest() {
        expertise = createEntity(em);
    }

    @Test
    @Transactional
    public void createExpertise() throws Exception {
        int databaseSizeBeforeCreate = expertiseRepository.findAll().size();

        // Create the Expertise
        ExpertiseDTO expertiseDTO = expertiseMapper.toDto(expertise);
        restExpertiseMockMvc.perform(post("/api/expertise")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expertiseDTO)))
            .andExpect(status().isCreated());

        // Validate the Expertise in the database
        List<Expertise> expertiseList = expertiseRepository.findAll();
        assertThat(expertiseList).hasSize(databaseSizeBeforeCreate + 1);
        Expertise testExpertise = expertiseList.get(expertiseList.size() - 1);
        assertThat(testExpertise.getSkill()).isEqualTo(DEFAULT_SKILL);
        assertThat(testExpertise.getExpertise()).isEqualTo(DEFAULT_EXPERTISE);
        assertThat(testExpertise.getAdditional()).isEqualTo(DEFAULT_ADDITIONAL);
    }

    @Test
    @Transactional
    public void createExpertiseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = expertiseRepository.findAll().size();

        // Create the Expertise with an existing ID
        expertise.setId(1L);
        ExpertiseDTO expertiseDTO = expertiseMapper.toDto(expertise);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExpertiseMockMvc.perform(post("/api/expertise")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expertiseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Expertise in the database
        List<Expertise> expertiseList = expertiseRepository.findAll();
        assertThat(expertiseList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllExpertise() throws Exception {
        // Initialize the database
        expertiseRepository.saveAndFlush(expertise);

        // Get all the expertiseList
        restExpertiseMockMvc.perform(get("/api/expertise?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(expertise.getId().intValue())))
            .andExpect(jsonPath("$.[*].skill").value(hasItem(DEFAULT_SKILL.toString())))
            .andExpect(jsonPath("$.[*].expertise").value(hasItem(DEFAULT_EXPERTISE.toString())))
            .andExpect(jsonPath("$.[*].additional").value(hasItem(DEFAULT_ADDITIONAL.toString())));
    }
    
    @Test
    @Transactional
    public void getExpertise() throws Exception {
        // Initialize the database
        expertiseRepository.saveAndFlush(expertise);

        // Get the expertise
        restExpertiseMockMvc.perform(get("/api/expertise/{id}", expertise.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(expertise.getId().intValue()))
            .andExpect(jsonPath("$.skill").value(DEFAULT_SKILL.toString()))
            .andExpect(jsonPath("$.expertise").value(DEFAULT_EXPERTISE.toString()))
            .andExpect(jsonPath("$.additional").value(DEFAULT_ADDITIONAL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExpertise() throws Exception {
        // Get the expertise
        restExpertiseMockMvc.perform(get("/api/expertise/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExpertise() throws Exception {
        // Initialize the database
        expertiseRepository.saveAndFlush(expertise);

        int databaseSizeBeforeUpdate = expertiseRepository.findAll().size();

        // Update the expertise
        Expertise updatedExpertise = expertiseRepository.findById(expertise.getId()).get();
        // Disconnect from session so that the updates on updatedExpertise are not directly saved in db
        em.detach(updatedExpertise);
        updatedExpertise
            .skill(UPDATED_SKILL)
            .expertise(UPDATED_EXPERTISE)
            .additional(UPDATED_ADDITIONAL);
        ExpertiseDTO expertiseDTO = expertiseMapper.toDto(updatedExpertise);

        restExpertiseMockMvc.perform(put("/api/expertise")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expertiseDTO)))
            .andExpect(status().isOk());

        // Validate the Expertise in the database
        List<Expertise> expertiseList = expertiseRepository.findAll();
        assertThat(expertiseList).hasSize(databaseSizeBeforeUpdate);
        Expertise testExpertise = expertiseList.get(expertiseList.size() - 1);
        assertThat(testExpertise.getSkill()).isEqualTo(UPDATED_SKILL);
        assertThat(testExpertise.getExpertise()).isEqualTo(UPDATED_EXPERTISE);
        assertThat(testExpertise.getAdditional()).isEqualTo(UPDATED_ADDITIONAL);
    }

    @Test
    @Transactional
    public void updateNonExistingExpertise() throws Exception {
        int databaseSizeBeforeUpdate = expertiseRepository.findAll().size();

        // Create the Expertise
        ExpertiseDTO expertiseDTO = expertiseMapper.toDto(expertise);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExpertiseMockMvc.perform(put("/api/expertise")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expertiseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Expertise in the database
        List<Expertise> expertiseList = expertiseRepository.findAll();
        assertThat(expertiseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteExpertise() throws Exception {
        // Initialize the database
        expertiseRepository.saveAndFlush(expertise);

        int databaseSizeBeforeDelete = expertiseRepository.findAll().size();

        // Delete the expertise
        restExpertiseMockMvc.perform(delete("/api/expertise/{id}", expertise.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Expertise> expertiseList = expertiseRepository.findAll();
        assertThat(expertiseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Expertise.class);
        Expertise expertise1 = new Expertise();
        expertise1.setId(1L);
        Expertise expertise2 = new Expertise();
        expertise2.setId(expertise1.getId());
        assertThat(expertise1).isEqualTo(expertise2);
        expertise2.setId(2L);
        assertThat(expertise1).isNotEqualTo(expertise2);
        expertise1.setId(null);
        assertThat(expertise1).isNotEqualTo(expertise2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExpertiseDTO.class);
        ExpertiseDTO expertiseDTO1 = new ExpertiseDTO();
        expertiseDTO1.setId(1L);
        ExpertiseDTO expertiseDTO2 = new ExpertiseDTO();
        assertThat(expertiseDTO1).isNotEqualTo(expertiseDTO2);
        expertiseDTO2.setId(expertiseDTO1.getId());
        assertThat(expertiseDTO1).isEqualTo(expertiseDTO2);
        expertiseDTO2.setId(2L);
        assertThat(expertiseDTO1).isNotEqualTo(expertiseDTO2);
        expertiseDTO1.setId(null);
        assertThat(expertiseDTO1).isNotEqualTo(expertiseDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(expertiseMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(expertiseMapper.fromId(null)).isNull();
    }
}
